(params) => {
	let name = params.name
	let cmds = {
		bob: ["salades","tomates", "oignons", "veau", "harissa"],
		sam:["salades","tomates","veau"],
		john: ["salade", "veau"],
		paul: ["salade","oignons", "poulet", "curry","frites"],
		babs: ["assiette-kebab", "salade", "tomates", "veau", "riz","harissa"]
	}
	return cmds[name]
}
